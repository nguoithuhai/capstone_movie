import React from "react";
import { useSelector } from "react-redux";
import { localUserServ } from "../../services/localServices";
import { NavLink } from "react-router-dom";

export default function HeaderDesktop() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    // remove local
    // đá ra login
    localUserServ.remove();
    window.location.href = "/";
  };

  let renderUserNav = () => {
    let btnCss = "px-5 py-2 rounded border-2 border-black";
    if (userInfo) {
      // đã đăng nhập
      return (
        <>
          <span>{userInfo.hoTen}</span>
          <button onClick={handleLogout} className={btnCss}>
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className={btnCss}>Đăng nhập</button>
          </NavLink>

          <NavLink to="/dangky">
            <button className={btnCss}>Đăng ký</button>
          </NavLink>
        </>
      );
    }
  };
  return (
    <div className="border-b border-black">
      <div className="flex justify-between h-20 items-center container m-auto ">
        <NavLink to="/">
          <span className="text-red-500 font-medium animate-bounce inline-block text-xl">
            CyberFlix
          </span>
        </NavLink>
        <div className="space-x-10 ">{renderUserNav()}</div>
      </div>
    </div>
  );
}

// viết hàm đăng xuất: remove localStorage, reload trang bằng window.location
