import React from "react";

import { Desktop, Mobile, Tablet } from "../../Layout/Respnsive";

import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";
import HeaderTablet from "./HeaderTablet";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDesktop /> {""}
      </Desktop>
      <Mobile>
        <HeaderMobile />
      </Mobile>
      <Tablet>
        <HeaderTablet />
      </Tablet>
    </div>
  );
}
