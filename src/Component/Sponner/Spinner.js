import React from "react";
import { useSelector } from "react-redux";
import { PacmanLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });

  return isLoading ? (
    <div
      style={{ background: "#ffb703" }}
      className="h-screen w-screen fixed top-0 left-0 z-50 flex justify-center items-center"
    >
      <PacmanLoader color="#8ecae6" size={200} speedMultiplier={1} />
    </div>
  ) : (
    <></>
  );
}
