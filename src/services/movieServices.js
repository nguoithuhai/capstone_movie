import axios from "axios";
import { BASE_URL, configHeaders } from "./ config";

export const movieServ = {
  getMovieList: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07`, {
      headers: configHeaders(),
    });
  },
  getMovieByTheater: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01`,
      {
        headers: configHeaders(),
      }
    );
  },
  getDetaiMovie: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
};
