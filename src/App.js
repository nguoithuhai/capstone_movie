import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";
import DetailPage from "./Page/DetailPage/DetailPage";
import Layout from "./Layout/Layout";
import Spinner from "./Component/Sponner/Spinner";
import Dangky from "./Page/Dangky/Dangky";

function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/dangky" element={<Dangky />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
