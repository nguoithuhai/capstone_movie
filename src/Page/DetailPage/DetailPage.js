import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../services/movieServices";

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    movieServ
      .getDetaiMovie(params.id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  return (
    <div>
      <div className="container w-screen h-screen bg-cover bg-no-repeat flex items-center justify-center border-8 border-blue-200">
        <div className="container flex items-center justify-center space-x-10">
          <div className="container ">
            <img src={movie.hinhAnh} className="w-60 border-1" alt="" />
            <NavLink to="/">
              <p className="border-1 w-60 text-lime-600 text-5xl">Home</p>
            </NavLink>
          </div>

          <Progress
            type="circle"
            percent={movie.danhGia * 10}
            status="exception"
            format={(percent) => {
              return percent / 10 + " điểm";
            }}
            size={200}
          />
          <div className="space-y-10">
            <h2 className="text-3xl text-cyan-900 font-medium hover:tracking-wider duration-150">
              {movie.tenPhim}
            </h2>
            <p className="text-cyan-600">{movie.moTa}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
