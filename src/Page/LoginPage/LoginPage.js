import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../../services/userServices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../../redux/constant/userConstant";
import { localUserServ } from "../../services/localServices";
import Lottie from "lottie-react";
import bg_animate from "../../assets/bg_animate.json";
import {
  setUserAction,
  setUserActionthunk,
} from "../../redux/action/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinishThunk = (value) => {
    let handleSuccess = (res) => {
      message.success("Đăng nhập thành công");
      localUserServ.set(res.data.content);
      navigate("/");
    };
    dispatch(setUserActionthunk(value, handleSuccess));
  };
  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        console.log(res);

        message.success("Đăng nhập thành công");
        localUserServ.set(res.data.content);
        // redux localStorage
        dispatch(setUserAction(res.data.content));
        /*
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
       */
        navigate("/");
        // đưa user về home page
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");

        console.log(err);
      });
  };
  //   abc123 123456
  //   message antd
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen bg-red-600 flex items-center justify-center">
      <div className=" rounded p-10 bg-white w-11/12 flex items-center justify-center">
        <di className="w-1/2 h-full">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type="primary" htmlType="submit" danger>
                Submit
              </Button>
            </Form.Item>
          </Form>
        </di>
        <di className="w-1/2 h-full">
          <Lottie animationData={bg_animate} loop={true} />
        </di>
      </div>
    </div>
  );
}
// tailwind react installation
