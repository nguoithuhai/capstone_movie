import { localUserServ } from "../../services/localServices";
import { USER_LOGIN } from "../constant/userConstant";

const initialState = {
  userInfo: localUserServ.get(),
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      state.userInfo = payload;
      return { ...state };

    default:
      return state;
  }
};
export default userReducer;
