import { BAT_LOADING, TAT_LOADING } from "../constant/spinnerContant";

const initialState = { isLoading: false };

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case BAT_LOADING:
      return { ...state, isLoading: true };
    case TAT_LOADING:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};
