import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function ChildrenTabMovie({ listMovie }) {
  return (
    <div style={{ height: 600 }} className="overflow-y-auto">
      {listMovie.map((phim) => {
        return (
          <div className="flex container p-1">
            <img
              style={{
                height: 120,
                width: 140,
                objectFit: "contain",
                // objectPsition: "top",
              }}
              alt=""
              src={phim.hinhAnh}
            />

            {/* <img className="w-24" src={phim.hinhAnh} alt="" /> */}
            <div>
              <p>{phim.tenPhim}</p>

              <div
                className="overflow-y-auto"
                style={{
                  fontSize: 14,
                  height: 120,
                  width: 200,
                }}
              >
                {phim.lstLichChieuTheoPhim.map((lich) => {
                  return (
                    <p>{moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}</p>
                  );
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
