import React, { Children, useEffect, useState } from "react";
import { movieServ } from "../../../services/movieServices";
import { Tabs } from "antd";
import ChildrenTabMovie from "./ChildrenTabMovie";

export default function TabMovie() {
  const [dataMovie, setdataMovie] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setdataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="" />,
        children: (
          <Tabs
            style={{ height: 600 }}
            //width của component cha
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.diaChi,
                label: <div className="w-48">{cumRap.tenCumRap}</div>,
                children: <ChildrenTabMovie listMovie={cumRap.danhSachPhim} />,
              };
            })}
          />
        ),
      };
    });
  };
  //   moment js npm
  return (
    <div className="container mt-10">
      <Tabs
        style={{ height: 600 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
      />
    </div>
  );
}
