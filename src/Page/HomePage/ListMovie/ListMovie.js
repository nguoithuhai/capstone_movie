import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  batloadingAction,
  tatloadingAction,
} from "../../../redux/action/spinnerAction";
import { movieServ } from "../../../services/movieServices";
import CardMovie from "./CardMovie";

export default function ListMovie() {
  let dispatch = useDispatch();
  const [movies, setMovies] = useState();
  useEffect(() => {
    //bật loading
    dispatch(batloadingAction());
    movieServ
      .getMovieList()
      .then((res) => {
        // TAT LOADING
        dispatch(tatloadingAction());

        setMovies(res.data.content);
      })
      .catch((err) => {
        // TAT LOADING
        dispatch(tatloadingAction());
        console.log(err);
      });
  }, []);
  return (
    <div className="grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-10 container mx-auto ">
      {/* grid col 5 tailwind + card: hinhAnh,tenPhim */}
      {/* optional chaining */}
      {movies?.map((item) => {
        return <CardMovie movie={item} />;
      })}
    </div>
  );
}
