import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const userServ = {
  postLogin: (formLogin) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: formLogin,
      headers: configHeaders(),
    });
  },
};
