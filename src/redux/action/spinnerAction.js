import { BAT_LOADING, TAT_LOADING } from "../constant/spinnerContant";

export const batloadingAction = (payload) => ({
  type: BAT_LOADING,
});
export const tatloadingAction = (payload) => ({
  type: TAT_LOADING,
});
