import { userServ } from "../../services/userServices";
import { USER_LOGIN } from "../constant/userConstant";

export const setUserAction = (value) => {
  return {
    typtype: USER_LOGIN,
    payload: value,
  };
};
// return 1 thm so dc rut gon bỏ return
/*
export const setUserAction = (value) => ({
  typtype: USER_LOGIN,
  payload: value,
});
*/

export const setUserActionthunk = (formdata, onSuccess) => {
  return (dispatch) => {
    userServ
      .postLogin(formdata)
      .then((res) => {
        console.log("res: ", res);
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
